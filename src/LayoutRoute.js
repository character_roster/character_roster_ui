import React, { PureComponent} from 'react'
import { Route } from 'react-router-dom'
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export default class LayoutRoute extends PureComponent {
    render() {
        const renderRoute = routeProps => {
            const { Component, ...rest} = this.props;
            return <Component {...routeProps} {...rest} />
        }
        return (
            <div>
            <Container>
                <Box my={4}>
                    <Typography variant="h4" component="h1" gutterBottom>
                        Character Roster
                    </Typography>
                    <Route {...this.props} render={renderRoute} />
                </Box>
            </Container>    
            </div>
        )
    }
}