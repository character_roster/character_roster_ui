import React from 'react';
import NewUserForm from './NewUserForm';
import UsersList from './UsersList';

export default () => (
    <div>
        <NewUserForm />
        <UsersList />
    </div>
);