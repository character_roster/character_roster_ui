import React, { useState } from 'react';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  }
}));

const headers = [
  { id: 'name', label: 'Name' },
  { id: 'email', label: 'Email' },
  { id: 'birthday', label: 'Birthday' },
  { id: 'zipcode', label: 'ZIP Code' }
];

export default function UsersListComponent({ users, sortUsers, deleteUser }) {
  const classes = useStyles();

  const [orderBy, setOrderBy] = useState('name');
  const [order, setOrder] = useState('asc');

  const createSortHandler = property => event => {
    handleRequestSort(event, property);
  };

  function handleRequestSort(event, property) {
    setOrderBy(property);
    sortUsers(property);
  }

  const createDeleteHandler = id => event => {
    deleteUser(id, orderBy);
  };

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            {headers.map(row => (
              <TableCell
                key={row.id}
              >
                <TableSortLabel
                  active={orderBy === row.id}
                  direction={order}
                  onClick={createSortHandler(row.id)}
                >
                  {row.label}
                </TableSortLabel>
              </TableCell>
            ))}           
            <TableCell></TableCell> 
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map(row => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell>{moment.utc(row.birthday).format('MM/DD/YYYY')}</TableCell>
              <TableCell>{row.zipcode}</TableCell>
              <TableCell>
                <Button onClick={createDeleteHandler(row.id)}>
                  <DeleteIcon/>
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}
