import * as UsersActions from './UsersActions';

const initialState = [];

export default function users(state = initialState, action) {
    switch (action.type) {
        case UsersActions.FETCH_USERS_COMPLETE:
            return action.users;
        case UsersActions.FETCH_USERS:
        default:
            return state;
    }
  }