import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from './lib/history'
import { usersReducer } from './app/Users'

export default combineReducers({
    router: connectRouter(history),
    users: usersReducer
});