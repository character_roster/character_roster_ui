import React from 'react';
import LayoutRoute from './LayoutRoute';
import Users from './app/Users'

export default [
    <LayoutRoute path="/users" Component={Users} />,
    <LayoutRoute path="/" Component={Users} />
];