import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import history from '../lib/history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import reducers from '../reducers';

export default initialState => {
    const middleware = [thunk];

    if (process.env.NODE_ENV !== 'production') {
        middleware.push(createLogger());
        middleware.push(routerMiddleware(history))
    }

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        connectRouter(history)(reducers),
        initialState,
        composeEnhancers(applyMiddleware(...middleware))
    );
};